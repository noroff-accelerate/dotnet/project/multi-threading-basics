# Multi Threading Basics

.NET Core Console Application

Intended for learning .NET multi-threading as part of module 1 of Noroff Accelerate .NET Fullstack short course.

Demonstrates the example use of multiple threads


## Getting Started

Clone to a local directory.

Open solution in Visual Studio

Run

### Prerequisites

.NET Framework

Visual Studio 2017/19 OR Visual Studio Code


## Authors

***Dean von Schoultz** [deanvons](https://gitlab.com/deanvons)





