﻿using System;
using System.Threading;
using System.Threading.Tasks;
namespace MultiThreadingBasics
{
    class Program
    {
        static void Main(string[] args)
        {
            //Run methods without threads you will notice a delay which did not occur when using threads
            Method1();
            Method2();

            // Creating and initializing threads. Notice that method 2 runs during the delay from method 1
            Thread thr1 = new Thread(Method1);
            Thread thr2 = new Thread(Method2);
            thr1.Start();
            thr2.Start();
        }

        // static method one 
        public static void Method1()
        {
            // It prints numbers from 0 to 10 
            for (int i = 0; i <= 10; i++)
            {
                Console.WriteLine("Method1 is : {0}", i);

                // When the value of I is equal to 5 then 
                // this method sleeps for 6 seconds 
                if (i == 5)
                {
                    Thread.Sleep(4000);
                }
            }
        }

        // static method two 
        public static void Method2()
        {
            // It prints numbers from 0 to 10 
            for (int j = 0; j <= 10; j++)
            {
                Console.WriteLine("Method2 is : {0}", j);
            }
        }

    }
}
